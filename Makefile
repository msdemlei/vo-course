MAIN_DOC = course.tex

SOURCES = $(MAIN_DOC) ADQL-course/course.tex pyvo-course/course.tex \
	slidetools/slidetools.cls

FIGURES = figures/*

SLIDETOOLS ?= https://codeberg.org/hendhd/slidetools

INSTALL_TARGET = hend@alnilam:/var/www/docs/vocourse/

-include slidetools/Makefile

export TEXINPUTS=.:slidetools:ADQL-course:pyvo-course:

slidetools/Makefile:
	@echo "*** initial clone of slidetools; use make update to update later."
	@echo
	git clone $(SLIDETOOLS)

ADQL-course/course.tex:
	git clone https://codeberg.org/hendhd/ADQL-course

pyvo-course/course.tex:
	git clone https://codeberg.org/msdemlei/pyvo-course

update-all:
	git pull
	(cd slidetools; git pull)
	(cd ADQL-course; git pull)
	(cd pyvo-course; git pull)

push-all:
	git push
	(cd slidetools; git push)
	(cd ADQL-course; git push)
	(cd pyvo-course; git push)

pull-all:
	git pull
	(cd slidetools; git pull)
	(cd ADQL-course; git pull)
	(cd pyvo-course; git pull)


status-all:
	git status
	(cd slidetools; git status)
	(cd ADQL-course; git status)
	(cd pyvo-course; git status)

%.pdf: %.pres $(SOURCES)
	$(TEX) -jobname=$* -pretex="\def\myMode{p}\def\justbuild{$*}" -usepretex course
	# I have zero idea why $*.[^p]* won't work, but it doesn't
	rm $*.[a-or-z]*
