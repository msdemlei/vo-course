A Course on the Virtual Observatory
===================================

The `Virtual Observatory`_ (VO) is a global infrastructure for
discovering, accessing, and using astronomical data.  This course,
first given at Universität Heidelberg in the summer semester 2024,
should give people with a basic knowledge of astronomy and programming a
solid foundation to exploit it for their research.  The official title
was “Einführung in das Virtuelle Observatorium (VO):
Konzepte, Sprachen, Anwendungen”.
While we certainly hope is is suitable for self-study, we would be happy
if this work inspired VO teaching in astronomical curricula elsewhere,
too.


.. _Virtual Observatory: https://ivoa.net

For pre-built PDFs, see `doi:10.21938/avVAxDlGOiu0Byv7NOZCsQ`_.  To
generate PDFs yourself, see Building_ below.

.. _doi:10.21938/avVAxDlGOiu0Byv7NOZCsQ: https://doi.org/10.21938/avVAxDlGOiu0Byv7NOZCsQ

All text and original imagery here is distributed under CC-0.  Science
data represented or mentioned here may be affected by copyright, though.

For questions and comments please use the issue tracker; we welcome
contributions of any kind.


Course Content
--------------

The main thread of the lecture covers:

* An appetiser with a brief demo involving the Registry, TAP, and
  basic workflows.

* A brief introduction to the “S-Protocols“ (SIAP, SSAP, SCS) and their
  clients.

* A longer introduction to ADQL, the Astronomical Data Query Langauge,
  and TAP, the Table Access Protocol.  This is designed to take about
  three sessions and covers basics including GROUP-ing and JOIN-s as
  well as a few more advanced topics such as cross matches when epoch
  propagation becomes necessary, or tricking the query planner when it
  is confused.

* An interlude on HEALPix and associated technologies (MOC, HiPS).

* The basics of pyVO, an interface between Python programs and the VO.

* Ways in which pyVO makes TAP and ADQL even more exciting, guided by a
  use case involving SED building.

* A use case for automating analysis software using SAMP, based on pyVO.

* A users' view on Datalink, a powerful protocol for publishing complex
  data, again based on pyVO.

* As a perspective on what is already marginally possible but quite likely
  still unreasonable, the course concludes with experiments in
  cross-server TAP queries.

* We will complement the course with a few slides on pyVO's new API for
  global dataset discovery as that becomes released.

In particular the ADQL course is also tried and tested as a standalone
course doable in a day or, at breakneck speed, an afternoon.  See
`doi:10.21938/uH0_xl5a6F7tKkXBSPnZxg`_.  A similar pull-out (if
admittedly less plausible) exists for pyVO, but we frankly do not forsee
that a linear stand-alone presentation of that course will actually work
better than just using the full course and skipping some optional parts:
`doi:10.21938/08rzo4ylRPmnS8iXYPO:rg`_.  Taking out a few subsections of
the pyVO course as part of an astronomer-oriented Python course might be
a good idea, though.

.. _doi:10.21938/08rzo4ylRPmnS8iXYPO:rg: https://doi.org/10.21938/08rzo4ylRPmnS8iXYPO:rg
.. _doi:10.21938/uH0_xl5a6F7tKkXBSPnZxg: https://doi.org/10.21938/uH0_xl5a6F7tKkXBSPnZxg


Side Tracks
'''''''''''

Some material can move around (almost) freely in the course, each covering
an overarching topic.  This is what the side tracks are about.  In our
Heidelberg lecture, we have used them to fill up lessons that went
faster than expected.  At this point, there are side tracks on:

* Terminology

* Architecture (as in: Why all this VO complication when people could
  have web pages instead?)

* Standards (including an overview of what standards there are and which
  ones not to read).

* UCDs (that's Unified Content Descriptors, short-ish strings denoting
  (largely) physical concepts in a semi-formal manner) and what they
  may be good for.

* Vocabularies (Hierarchical lists of concepts and metadata for them)
  and how we deal with them in the VO.

* VOTable, the VO's XML-based container for data and metadata.

* IVOA Identifiers – what these pesky URIs with an ivo: scheme really
  mean and how to resolve them.


Imported Stuff
--------------

The docrepo.bib bibliography and the corresponding ivoa.bst is taken
from ivoatex_ and should occasionally be updated from there.

.. _ivoatex: http://ivoatex.g-vo.org


Building
-----------------

The way this *should* work is that you say, with git, GNU make, and
something like TeXLive installed::

  git clone https://codeberg-msdemlei/msdemlei/vo-course.git
  cd vo-course
  make

If it does not for you, please file a bug.

Both the ADQL course and the pyVO course are designed to work
stand-alone given suitable audiences.  They are hence pulled in from
separate repositories.  This is a bit clumsy in terms of pushing and
pulling, which is why I have added the ``status-all``, ``pull-all`` and
``push-all`` make targets.

Standard build targets:

* slides.pdf (the default): builds slides for use in the lecture
* exercises.pdf: builds slides of the lab sessions, containing the
  problems, one per slide
* notes.pdf: builds the lecture notes
* install: puts the lecture notes to some place useful for the authors.
  This will probably need to be made configurable at some point.
* Individual lectures: These may be more convenient in the lecture
  situation because you don't have to render all the slides when you
  just need a handful.

  To build these individual lectures, there is this pattern rule::

    %.pdf: %.pres $(SOURCES)
	  $(TEX) -jobname=$* -pretex="\def\myMode{p}\def\justbuild{$*}" -usepretex course
	  rm -f $*.[^p]*

  The idea is that when you have a lecture, you create an empty file with
  the label of the lecture and the extension ``.pres``, say,
  ``label.pres``.  After that, you can say ``make label.pdf`` and build
  the slides for that lecture.  The .pres-es for the current lectures are
  in version control.
