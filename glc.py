import pyvo
from astropy import table


def send_position_to(conn, ra, dec, client_name=None):
	"""sends ra, dec as the position to focus to a client (or does a broadcast
	for client None).

	This is only here as long as it's missing in pyvo.samp.
	"""
	message = {
		"samp.mtype": "coord.pointAt.sky",
		"samp.params": {
			"ra": str(ra),
			"dec": str(dec),
		}}
	if client_name is None:
		conn.notify_all(message)
	else:
		client_id = find_client_id(conn, client_name)
		conn.notify(client_id, message)




TAP_URL = "http://dc.zah.uni-heidelberg.de/tap"
QUERY = """
SELECT
name,
db.ra, db.dec, u, z, photoz_z,
	petrads, petmags
  FROM sdssdr16.main AS db
  JOIN TAP_UPLOAD.t3 AS tc
  ON 1=CONTAINS(POINT('ICRS', db.ra, db.dec),
    CIRCLE('ICRS', tc.ra, tc.dec, 240./3600.))
WHERE u-z>6
"""

svc = pyvo.dal.TAPService(TAP_URL)
objs = svc.run_sync(QUERY,
	uploads={"t3": table.Table.read("candclus.vot")}).to_table()


# Here's where you put in your own logic.  Which isn't very logical here.
def argmax(s):
	return s.index(max(s))

with pyvo.samp.connection() as conn:
	for o in objs:
		pr = o["petrads"]
		if (sum(pr)>40 and min(pr)*6<max(pr)
				and argmax(list(o["petmags"]))==1):
			print(f"Showing a candidate around {o['name']}")
			send_position_to(conn, o["ra"], o["dec"])
			input()
